import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/model/Message.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:ahoko_chat_app/rest/BackendServiceImpl.dart';
import 'package:ahoko_chat_app/rest/exception/AuthException.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';

void main(){

  final BackendService _backendService = new BackendServiceImpl();

  test('login user correctly', ()async{
    final Account result = await _backendService.loginAccount(email:"angebagui@adjemincloud.com", password:'123456789');
    expect(result.runtimeType, Account);

  });

  test('login with bad password', ()async{
    try{
      final Account result = await _backendService.loginAccount(email:"angebagui@adjemincloud.com", password:'12345678');

    }catch(error){
      expect(error.runtimeType, AuthException);
    }


  });

  /*test('register user correctly', ()async{
    final Account result = await _backendService.registerAccount(
        firstName: 'Test',
        lastName: 'Test',
        email:"angebagui${DateTime.now().minute}@adjemincloud.com",
        password:'123456789'
    );
    expect(result.runtimeType, Account);

  });

  test('register user when email already exist', ()async{
    try{
      final Account result = await _backendService.registerAccount(
          firstName: 'Test',
          lastName: 'Test',
          email:"angebagui4@adjemincloud.com",
          password:'123456789'
      );
    }catch(error){
      expect(error.runtimeType, AuthException);
    }

  });

  test('register user when email and password is required', ()async{
    try{
      final Account result = await _backendService.registerAccount(
          firstName: 'Test',
          lastName: 'Test',
          email:"",
          password:''
      );
    }catch(error){

      expect(error is Response && error.statusCode == 500, true);
    }

  });*/


  test('load contacts successfully', ()async{
    try{
      final contacts = await _backendService.loadContactsByAccountID(91);
      print("Contacts =>>> $contacts");
    expect(contacts.runtimeType, <Account>[].runtimeType);
    }catch(error){

    }
  });

  test('Open Conversation successfully', ()async{
    try{
      final conversation = await _backendService.findOrCreateConversation(
          speakers: [50,75],
          isGroup:false,
          groupName:"",
          admins:[]
      );
      print("Conversation =>>> $conversation");
      expect(conversation.runtimeType, Conversation);
    }catch(error){

    }
  });

  test('load messages by conversation successfully', ()async{
    try{
      final messages = await _backendService.loadMessagesByConversationByID(40);
      print("Messages =>>> $messages");
      expect(messages.runtimeType, <Message>[].runtimeType);
    }catch(error){

    }
  });

  test('load conversations by accountID successfully', ()async{
    try{
      final conversations = await _backendService.loadConversationsByAccountID(50);
      print("Conversations =>>> $conversations");
      expect(conversations.runtimeType, <Conversation>[].runtimeType);
    }catch(error){

    }
  });

  test('send Message successfully', ()async{
    try{
      final message = await _backendService.sendMessage(
          content:'Hello Boy',
          contentType:'text',
          senderId:50,
          conversationId:42
      );
      print("Message =>>> $message");
      expect(message.runtimeType, Message);
    }catch(error){

    }
  });


}