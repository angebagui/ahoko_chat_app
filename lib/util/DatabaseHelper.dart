import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;


class DatabaseHelper{

  static final String TABLE_ACCOUNTS = "accounts";

  static  Database _db;

  static Future<Database> get db async{

    if(_db == null ){
      _db = await _iniDb();
    }

    return _db;

  }

  static Future<Database> _iniDb()async {

    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = p.join(databasesPath, 'ahoko_chat_app_dev.db');

    // open the database
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          // When creating the db, create the table
          await db.execute(
              'CREATE TABLE $TABLE_ACCOUNTS (id INTEGER PRIMARY KEY, data TEXT)');
        });

    print(">>>>>>>>>>>>>>>>>>>>>  TABLES CREATED >>>>>>>>>>>>>");

    return database;
  }

  static  Future<int> insertUser(Map<String, dynamic> json) async{
    print(">>>>>>>>>>>>>>>>>>>>>  insertUser >>> STARTED  >>>>>>>>>>>>>");
    final Database dbClient = await db;
    final result =  await dbClient.insert(TABLE_ACCOUNTS,json);
    print(">>>>>>>>>>>>>>>>>>>>>  insertUser >>> $result  >>>>>>>>>>>>>");
    print(">>>>>>>>>>>>>>>>>>>>>  insertUser >>> ENDED  >>>>>>>>>>>>>");
    return result;

  }

  static Future<int> deleteUsers() async{
    print(">>>>>>>>>>>>>>>>>>>>>  deleteUsers >>> STARTED  >>>>>>>>>>>>>");
    final Database dbClient = await db;
    final result =  await dbClient.rawDelete("DELETE FROM $TABLE_ACCOUNTS");
    print(">>>>>>>>>>>>>>>>>>>>>  deleteUsers >>> $result  >>>>>>>>>>>>>");
    print(">>>>>>>>>>>>>>>>>>>>>  deleteUsers >>> ENDED  >>>>>>>>>>>>>");
    return result;

  }


  static Future<List<Map<String, dynamic>>> allUsers(int limit) async{
    print(">>>>>>>>>>>>>>>>>>>>>  allUsers >>> STARTED  >>>>>>>>>>>>>");
    final Database dbClient = await db;
    final result =  await dbClient.query(TABLE_ACCOUNTS,limit: limit);
    print(">>>>>>>>>>>>>>>>>>>>>  allUsers >>> $result  >>>>>>>>>>>>>");
    print(">>>>>>>>>>>>>>>>>>>>>  allUsers >>> ENDED  >>>>>>>>>>>>>");
    return result;

  }


}
