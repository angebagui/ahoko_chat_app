import 'package:flutter/material.dart';

class AhokoIcons{
  static final logo = Image.asset('assets/images/logo_ahoko.png');

  static final message = Image.asset('assets/images/message.png', color: Colors.grey);
  static final add_box = Image.asset('assets/images/add_box.png', color: Colors.grey);
  static final friends = Image.asset('assets/images/friends.png', color: Colors.grey);
  static final notifications = Image.asset('assets/images/notifications.png', color: Colors.grey, height: 40,);
  static final profile = Image.asset('assets/images/profile.png', color: Colors.grey);

  static final message_active = Image.asset('assets/images/message.png', color: Colors.black,);
  static final add_box_active = Image.asset('assets/images/add_box.png', color: Colors.black,);
  static final friends_active = Image.asset('assets/images/friends.png', color: Colors.black);
  static final notifications_active = Image.asset('assets/images/notifications.png', color: Colors.black, height: 40,);
  static final profile_active = Image.asset('assets/images/profile.png', color: Colors.black);

}