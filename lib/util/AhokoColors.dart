import 'package:flutter/cupertino.dart';

class AhokoColors{

  static final Color primaryColor = Color(0xFF1ddb37);
  static final Color accentColor = Color(0xFF000000);
  static final Color online = Color(0xFF00F86B);
  static final Color disconnected = Color(0xFFD5D4D4);

  static final Color conversationTimeColor = Color(0xFF9F9F9F);
  static final Color onlineUserColor= Color(0xFF00F86B);


}