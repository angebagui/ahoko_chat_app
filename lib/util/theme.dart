import 'package:flutter/material.dart';

import 'AhokoColors.dart';
import 'AhokoFonts.dart';

ThemeData lightTheme(){
  return ThemeData(
    // This is the theme of your application.
    //
    // Try running your application with "flutter run". You'll see the
    // application has a blue toolbar. Then, without quitting the app, try
    // changing the primarySwatch below to Colors.green and then invoke
    // "hot reload" (press "r" in the console where you ran "flutter run",
    // or simply save your changes to "hot reload" in a Flutter IDE).
    // Notice that the counter didn't reset back to zero; the application
    // is not restarted.
      fontFamily: AhokoFonts.ubuntu,
      primaryColor: AhokoColors.primaryColor,
      accentColor: AhokoColors.accentColor
  );
}
ThemeData darkTheme(){
  return ThemeData(
    // This is the theme of your application.
    //
    // Try running your application with "flutter run". You'll see the
    // application has a blue toolbar. Then, without quitting the app, try
    // changing the primarySwatch below to Colors.green and then invoke
    // "hot reload" (press "r" in the console where you ran "flutter run",
    // or simply save your changes to "hot reload" in a Flutter IDE).
    // Notice that the counter didn't reset back to zero; the application
    // is not restarted.
      fontFamily: AhokoFonts.ubuntu,
      primaryColor: AhokoColors.primaryColor,
      accentColor: AhokoColors.accentColor
  );
}