import 'package:ahoko_chat_app/CompositionRoot.dart';
import 'package:flutter/material.dart';
import 'package:ahoko_chat_app/util/theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async{

  WidgetsFlutterBinding.ensureInitialized();

  CompositionRoot.init();

  final firstScreen = await CompositionRoot.start();

  runApp(new MyApp(firstScreen));
}

class MyApp extends StatelessWidget {
  Widget firstScreen;
  MyApp(this.firstScreen);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en', ''), // English, no country code
        Locale('fr', ''), // French, no country code
      ],

      theme: lightTheme(),
      darkTheme: darkTheme(),
      home: firstScreen,
    );
  }
}

