import 'package:ahoko_chat_app/data/SharedPrefs.dart';
import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/providers/ContactProvider.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:ahoko_chat_app/rest/BackendServiceImpl.dart';
import 'package:ahoko_chat_app/state_management/conversation/conversation_bloc.dart';
import 'package:ahoko_chat_app/state_management/login/LoginBloc.dart';
import 'package:ahoko_chat_app/state_management/messages/message_bloc.dart';
import 'package:ahoko_chat_app/state_management/register/RegisterBloc.dart';
import 'package:ahoko_chat_app/ui/auth/login/LoginScreen.dart';
import 'package:ahoko_chat_app/ui/auth/register/RegisterScreen.dart';
import 'package:ahoko_chat_app/ui/contacts/ListContactScreen.dart';
import 'package:ahoko_chat_app/ui/conversation/ConversationScreen.dart';
import 'package:ahoko_chat_app/ui/home/HomeScreen.dart';
import 'package:ahoko_chat_app/ui/home/tabs/MessageScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ahoko_chat_app/data/LocalStorageService.dart';
import 'package:provider/provider.dart';

class CompositionRoot{

  static BackendService _backendService;
  static LocalStorageService _localStorageService;
  static init(){
    _backendService = BackendServiceImpl();
   // _localStorageService = DatabaseManager();
    _localStorageService = SharedPrefs();
  }

  static Future<Widget> start()async{
    //Try to retrieve connected user
    final Account me = await _localStorageService.connectedUser();
    return me == null?loginScreen() : homeScreen(me);
  }



  static Widget loginScreen(){

    return BlocProvider(
      create: (_)=> LoginBloc(_backendService, _localStorageService),
      child: LoginScreen(),
    );
  }

  static Widget registerScreen({String email, String password}){

    return BlocProvider(
      create: (_)=> RegisterBloc(_backendService, _localStorageService),
      child: RegisterScreen(
          email:email,
          password:password
      ),
    );
  }

  static Widget homeScreen(Account me){
    return BlocProvider(
      create: (_)=> ConversationBloc(_backendService),
      child: HomeScreen(me),
    );
  }

  static Widget messageScreen(Account account){
    final MessageBloc _registerBloc = MessageBloc(_backendService);
    return BlocProvider(
      create: (_) => _registerBloc,
      child: MessageScreen(account),
    );
  }

  static Widget listContactScreen(Account account){

    return ChangeNotifierProvider<ContactProvider>(
      create: (_) => ContactProvider(_backendService),
      child: ListContactScreen(account),
    );
  }

  static Widget conversationScreen(Conversation conversation, Account account){

    final ConversationBloc _conversationBloc = ConversationBloc(_backendService);
    return BlocProvider(
      create: (_) => _conversationBloc,
      child: ConversationScreen(conversation,account),
    );
  }





}