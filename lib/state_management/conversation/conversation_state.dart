import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/model/Message.dart';

abstract class ConversationState{}

class ConversationInitial extends ConversationState{}


class ConversationFound extends ConversationState{
  final Conversation conversation;
  ConversationFound(this.conversation);
}

class MessageSent extends ConversationState{
  final Message message;
  MessageSent(this.message);
}

class MessagesFound extends ConversationState{
  final List<Message> messages;
  MessagesFound(this.messages);
}

class Failure extends ConversationState{
  final dynamic error;
  Failure(this.error);
}
