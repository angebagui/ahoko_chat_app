import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:bloc/bloc.dart';

import 'conversation_event.dart';
import 'conversation_state.dart';

class ConversationBloc extends Bloc<ConversationEvent, ConversationState>{
  final BackendService _backendService;
  ConversationBloc(this._backendService) : super(ConversationInitial());

  @override
  Stream<ConversationState> mapEventToState(ConversationEvent event) async*{
    if(event is FindConversation){

      try{
        final conversation =  await _backendService.findOrCreateConversation(
            speakers: event.speakers,
            isGroup: false
        );

        yield ConversationFound(conversation);
      }catch(error){
        yield Failure(error);
      }

    }

    if(event is FindMessagesByConversation){

      try{
        final messages =  await _backendService.loadMessagesByConversationByID(
            event.conversationId
        );

        yield MessagesFound(messages);
      }catch(error){
        yield Failure(error);
      }

    }

    if(event is SendMessage){

      try{
        final message =  await _backendService.sendMessage(
            content: event.message.content,
            contentType: event.message.contentType,
            senderId: event.message.senderId,
            conversationId: event.message.conversationId
        );

        yield MessageSent(message);
      }catch(error){
        yield Failure(error);
      }

    }

  }

}
