abstract class RegisterEvent{}

class RegisterSubmission extends RegisterEvent{
  final String email;
  final String password;
  final String firstName;
  final String lastName;
  RegisterSubmission(this.email, this.password, this.firstName, this.lastName);

}