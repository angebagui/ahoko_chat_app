import 'package:ahoko_chat_app/model/Account.dart';

abstract class RegisterState{}

class RegisterInitial extends RegisterState {}

class RegisterProgress extends RegisterState {}

class RegisterSucceeded extends RegisterState {
  final Account account;
  RegisterSucceeded(this.account);
}

class RegisterFailed extends RegisterState{
  final dynamic error;
  RegisterFailed(this.error);
}