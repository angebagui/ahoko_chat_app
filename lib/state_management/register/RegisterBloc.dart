import 'package:ahoko_chat_app/data/LocalStorageService.dart';
import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:ahoko_chat_app/state_management/register/RegisterEvent.dart';
import 'package:ahoko_chat_app/state_management/register/RegisterState.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState>{
  final BackendService _backendService;
  final LocalStorageService _localStorageService;
  RegisterBloc(this._backendService, this._localStorageService) : super(RegisterInitial());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async*{

    if(event is RegisterSubmission){
      yield RegisterProgress();

      try{
        final Account account = await this._backendService.registerAccount(
            email: event.email,
            password: event.password,
          firstName: event.firstName,
          lastName: event.lastName
        );
        this._localStorageService.store(account);
        yield RegisterSucceeded(account);
      }catch(error){
        print("Error found $error");
        yield RegisterFailed(error);
      }


    }
  }

}