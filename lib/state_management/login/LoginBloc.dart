import 'package:ahoko_chat_app/data/LocalStorageService.dart';
import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:ahoko_chat_app/state_management/login/LoginEvent.dart';
import 'package:ahoko_chat_app/state_management/login/LoginState.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState>{
  final BackendService _backendService;
  final LocalStorageService _localStorageService;
  LoginBloc(this._backendService, this._localStorageService) : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async*{

    if(event is LoginSubmission){
      yield LoginProgress();

      try{
        final Account account = await this._backendService.loginAccount(
            email: event.email,
            password: event.password
        );
        this._localStorageService.store(account);
        yield LoginSucceeded(account);
      }catch(error){
        print("Error found $error");
        yield LoginFailed(error);
      }


    }
  }

}