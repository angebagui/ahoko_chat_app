import 'package:ahoko_chat_app/model/Account.dart';

abstract class LoginState{}

class LoginInitial extends LoginState {}

class LoginProgress extends LoginState {}

class LoginSucceeded extends LoginState {
  final Account account;
  LoginSucceeded(this.account);
}

class LoginFailed extends LoginState{
  final dynamic error;
  LoginFailed(this.error);
}