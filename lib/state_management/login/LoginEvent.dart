abstract class LoginEvent{}

class LoginSubmission extends LoginEvent{
  final String email;
  final String password;

  LoginSubmission(this.email, this.password);

}