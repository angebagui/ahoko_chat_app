import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MessageBloc extends Cubit<List<Conversation>>{
  final BackendService _backendService;

  MessageBloc(this._backendService) : super([]);

  void loadConversations(int accountID){

    _backendService.loadConversationsByAccountID(accountID)
        .then((value) =>  emit(value))
        .catchError((onError){
      print("Error found $onError");
    });

  }
}
