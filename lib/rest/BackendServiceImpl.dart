import 'dart:convert';

import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/model/Message.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:ahoko_chat_app/rest/exception/AuthException.dart';
import 'package:ahoko_chat_app/util/Constants.dart';
import 'package:http/http.dart';

class BackendServiceImpl implements BackendService{

  @override
  Future<Account> loginAccount({String email, String password})async {
    final url = Uri.parse("${Constants.API_BASE_URL}user_auth");
    final Map<String, String> forms = {
      'email':email,
      'password':password
    };
   final Response response = await post(url, body:forms);
   if(response.statusCode == 200){

     if(response.headers['content-type'] == 'application/json'){
       final jsonString = response.body;

       final Map<String, dynamic>  json = jsonDecode(jsonString);
       final bool success = json['success'];
       if(success){
         final Map<String, dynamic> dataJson = json['data'];
         print("ResponseBody JSON Connected User =>>> $dataJson");
         if(dataJson != null){
           return Account.fromJson(dataJson);
         }

       }
     }



   } else if(response.statusCode >= 400 && response.statusCode <500){
     if(response.headers['content-type'] == 'application/json'){
       final jsonString = response.body;
       final Map<String, dynamic>  json = jsonDecode(jsonString);
       print("ResponseBody JSON =>>> $json");
       final String message = json['message'];
       throw AuthException(message);
     }
   }else{

     throw response;

   }



  }

  @override
  Future<Account> registerAccount({String firstName, String lastName, String email, String password}) async{
    final url = Uri.parse("${Constants.API_BASE_URL}user_register");
    final Map<String, String> forms = {
      'first_name':firstName,
      'last_name':lastName,
      'email':email,
      'password':password
    };
    final Response response = await post(url, body:forms);
    if(response.statusCode == 200){

      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;

        final Map<String, dynamic>  json = jsonDecode(jsonString);
        final bool success = json['success'];
        if(success){
          final Map<String, dynamic> dataJson = json['data'];
          print("ResponseBody JSON Connected User =>>> $dataJson");
          if(dataJson != null){
            return Account.fromJson(dataJson);
          }

        }
      }



    } else if(response.statusCode >= 400 && response.statusCode <500){
      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;
        final Map<String, dynamic>  json = jsonDecode(jsonString);
        print("ResponseBody JSON =>>> $json");
        final String message = json['message'];
        throw AuthException(message);
      }
    }else{
      print("ResponseStatusCode =>>> ${response.statusCode}");
      print("ResponseBody =>>> ${response.body}");
      throw response;

    }
  }



  @override
  Future<List<Account>> loadContactsByAccountID(int accountID) async{
    final url = Uri.parse("${Constants.API_BASE_URL}contacts/$accountID");
    final Response response = await get(url);
    if(response.statusCode == 200){

      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;

        final Map<String, dynamic>  json = jsonDecode(jsonString);
        final bool success = json['success'];
        if(success){
          final List dataJson = json['data'];
          if(dataJson != null){
            return dataJson.map<Account>((json) => Account.fromJson(json as Map)).toList();
          }

        }
      }

    } else if(response.statusCode >= 400 && response.statusCode <500){
      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;
        final Map<String, dynamic>  json = jsonDecode(jsonString);
        print("ResponseBody JSON =>>> $json");
        final String message = json['message'];
        throw Exception(message);
      }
    }else{
      print("ResponseStatusCode =>>> ${response.statusCode}");
      print("ResponseBody =>>> ${response.body}");
      throw response;

    }
  }

  @override
  Future<Conversation> findOrCreateConversation({List<int> speakers, String groupName, bool isGroup, List<int> admins}) async{
    final url = Uri.parse("${Constants.API_BASE_URL}open_conversation");
    final Map<String, dynamic> forms = {
      "speakers": speakers,
      "is_group":isGroup,
      "group_name":groupName,
      "admins":admins

    };
    final Response response = await post(url,
        body: jsonEncode(forms),
       headers: {
        'Accept':'application/json',
        'Content-Type': 'application/json'
       }
    );
    if(response.statusCode == 200){

      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;

        final Map<String, dynamic>  json = jsonDecode(jsonString);
        final bool success = json['success'];
        if(success){
          final Map<String, dynamic> dataJson = json['data'];
          if(dataJson != null){
            return Conversation.fromJson(dataJson);
          }

        }
      }



    } else if(response.statusCode >= 400 && response.statusCode <500){
      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;
        final Map<String, dynamic>  json = jsonDecode(jsonString);
        print("ResponseBody JSON =>>> $json");
        final String message = json['message'];
        throw Exception(message);
      }
    }else{
      print("ResponseStatusCode =>>> ${response.statusCode}");
      print("ResponseBody =>>> ${response.body}");
      throw response;

    }
  }

  @override
  Future<List<Conversation>> loadConversationsByAccountID(int accountID)async {
    final url = Uri.parse("${Constants.API_BASE_URL}conversations/customers/$accountID");
    final Response response = await get(url);
    if(response.statusCode == 200){

      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;

        final Map<String, dynamic>  json = jsonDecode(jsonString);
        final bool success = json['success'];
        if(success){
          final List dataJson = json['data'];
          if(dataJson != null){
            return dataJson.map<Conversation>((json) => Conversation.fromJson(json as Map)).toList();
          }

        }
      }

    } else if(response.statusCode >= 400 && response.statusCode <500){
      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;
        final Map<String, dynamic>  json = jsonDecode(jsonString);
        print("ResponseBody JSON =>>> $json");
        final String message = json['message'];
        throw Exception(message);
      }
    }else{
      print("ResponseStatusCode =>>> ${response.statusCode}");
      print("ResponseBody =>>> ${response.body}");
      throw response;

    }
  }

  @override
  Future<List<Message>> loadMessagesByConversationByID(int conversationID) async{
    final url = Uri.parse("${Constants.API_BASE_URL}conversations/messages/$conversationID");
    final Response response = await get(url);
    if(response.statusCode == 200){

      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;

        final Map<String, dynamic>  json = jsonDecode(jsonString);
        final bool success = json['success'];
        if(success){
          final List dataJson = json['data'];
          if(dataJson != null){
            return dataJson.map<Message>((json) => Message.fromJson(json as Map)).toList();
          }

        }
      }

    } else if(response.statusCode >= 400 && response.statusCode <500){
      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;
        final Map<String, dynamic>  json = jsonDecode(jsonString);
        print("ResponseBody JSON =>>> $json");
        final String message = json['message'];
        throw Exception(message);
      }
    }else{
      print("ResponseStatusCode =>>> ${response.statusCode}");
      print("ResponseBody =>>> ${response.body}");
      throw response;

    }
  }

  @override
  Future<Message> sendMessage({String content, String contentType, int senderId, int conversationId})async {
    final url = Uri.parse("${Constants.API_BASE_URL}messages");
    final Map<String, String> forms = {
      "content": content,
      "content_type":contentType,
      "sender_id":'$senderId',
      "conversation_id":'$conversationId'
    };
    final Response response = await post(url,
        body: forms,
        headers: {
          'Accept':'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        }
    );
    if(response.statusCode == 200){

      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;

        final Map<String, dynamic>  json = jsonDecode(jsonString);
        final bool success = json['success'];
        if(success){
          final Map<String, dynamic> dataJson = json['data'];
          if(dataJson != null){
            return Message.fromJson(dataJson);
          }

        }
      }



    } else if(response.statusCode >= 400 && response.statusCode <500){
      if(response.headers['content-type'] == 'application/json'){
        final jsonString = response.body;
        final Map<String, dynamic>  json = jsonDecode(jsonString);
        print("ResponseBody JSON =>>> $json");
        final String message = json['message'];
        throw Exception(message);
      }
    }else{
      print("ResponseStatusCode =>>> ${response.statusCode}");
      print("ResponseBody =>>> ${response.body}");
      throw response;

    }
  }

}