import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/model/Message.dart';
import 'package:flutter/material.dart';

abstract class BackendService {


  Future<Account> registerAccount({
    String firstName,
    String lastName,
    String email,
    String password});

  Future<Account> loginAccount({
    String email,
    String password});

  /**
   * POST Open Conversation
   * https://documenter.getpostman.com/view/479468/UUxukAiF#2c88dfbf-303b-445d-a3bf-87531349b85e
   */
  Future<Conversation> findOrCreateConversation({@required List<int> speakers, String groupName, @required bool  isGroup, List<int> admins});

  /**
   * GET Load Conversations by Cusomter ID
   * https://documenter.getpostman.com/view/479468/UUxukAiF#45ea1481-4577-4e18-a8ba-afdb5e934cec
   */
  Future<List<Conversation>> loadConversationsByAccountID(int accountID);

  /**
   * GET Load Messages by Conversation ID
   * https://documenter.getpostman.com/view/479468/UUxukAiF#59f51239-11f4-406a-99a0-9b50ae1079f8
   */
  Future<List<Message>> loadMessagesByConversationByID(int conversationID);

  /**
   * POST Send message
   * https://documenter.getpostman.com/view/479468/UUxukAiF#a72ccf6b-acd7-4cbc-9023-d4d3547a1ac7
   */
  Future<Message> sendMessage({@required String content, @required String contentType, @required int senderId, @required int conversationId,});

  /**
   * GET Load All Contacts
   * https://documenter.getpostman.com/view/479468/UUxukAiF#ba0074b9-b8c0-43ce-949e-f692ab030cb2
   */
  Future<List<Account>> loadContactsByAccountID(int accountID);


}