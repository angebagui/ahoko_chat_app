import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/model/Message.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';

class FirebaseService implements BackendService{
  @override
  Future<Conversation> findOrCreateConversation({List<int> speakers, String groupName, bool isGroup, List<int> admins}) {
    // TODO: implement findOrCreateConversation
    throw UnimplementedError();
  }

  @override
  Future<List<Account>> loadContactsByAccountID(int accountID) {
    // TODO: implement loadContactsByAccountID
    throw UnimplementedError();
  }

  @override
  Future<List<Conversation>> loadConversationsByAccountID(int accountID) {
    // TODO: implement loadConversationsByAccountID
    throw UnimplementedError();
  }

  @override
  Future<List<Message>> loadMessagesByConversationByID(int conversationID) {
    // TODO: implement loadMessagesByConversationByID
    throw UnimplementedError();
  }

  @override
  Future<Account> loginAccount({String email, String password}) {
    // TODO: implement loginAccount
    throw UnimplementedError();
  }

  @override
  Future<Account> registerAccount({String firstName, String lastName, String email, String password}) {
    // TODO: implement registerAccount
    throw UnimplementedError();
  }

  @override
  Future<Message> sendMessage({String content, String contentType, int senderId, int conversationId}) {
    // TODO: implement sendMessage
    throw UnimplementedError();
  }

}