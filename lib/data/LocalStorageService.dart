import 'package:ahoko_chat_app/model/Account.dart';

abstract class LocalStorageService{

  Future<void> store(Account account);
  Future<Account> connectedUser();  

}
