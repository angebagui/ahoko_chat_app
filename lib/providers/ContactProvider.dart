import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/rest/BackendService.dart';
import 'package:flutter/material.dart';

class ContactProvider extends ChangeNotifier{

  final BackendService _backendService;

  ContactProvider(this._backendService);

  List<Account> _allContacts = [];

  loadContacts(int accountID){
    _backendService.loadContactsByAccountID(accountID)
        .then((List<Account> value) => setData(value))
        .catchError((onError)=> print("Error found $onError"));
  }

  setData(List<Account> list){
    _allContacts = list;
    notifyListeners();
  }

  List<Account> get allContacts => _allContacts;

}
