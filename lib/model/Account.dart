import 'package:json_annotation/json_annotation.dart';
part 'Account.g.dart';

@JsonSerializable()
class Account{

  final int id;
  @JsonKey(name: 'first_name',nullable: true)
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  final String email;
  final String password;
  final String photo;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;

  const Account(
    this.id,
    this.firstName,
    this.lastName,
    this.password,
    this.email,
    this.photo,
    this.createdAt,
    this.updatedAt
  );

  factory Account.fromJson(Map<String, dynamic> json) => _$AccountFromJson(json);
  Map<String, dynamic> toJson() => _$AccountToJson(this);

/*
  static Account fromJson(Map json) => Account(
    id: json['id'],
    firstName: json['first_name'],
    lastName:  json['last_name'],
    password:  json['password'],
    email:  json['email'],
    photo:  json['photo'],
    createdAt: json['created_at'] ==null? null: DateTime.parse(json['created_at']),
    updatedAt:  json['updated_at'] ==null? null:DateTime.parse(json['updated_at']),
  );

  Map<String, dynamic> toJson() => {
  'id': this.id,
  'first_name': this.firstName,
  'last_name': this.lastName,
  'email': this.email,
  'password': this.password,
  'photo': this.photo,
  'created_at': this.createdAt==null? null: this.createdAt.toIso8601String(),
  'updated_at': this.updatedAt==null? null: this.updatedAt.toIso8601String()
  };*/

  @override
  String toString() {
    return 'Account{id: $id, firstName: $firstName, lastName: $lastName, email: $email, password: $password, photo: $photo, createdAt: $createdAt, updatedAt: $updatedAt}';
  }
}