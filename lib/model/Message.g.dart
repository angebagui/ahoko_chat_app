// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message(
    id: json['id'] as int,
    content: json['content'] as String,
    senderId: json['sender_id'] as int,
    conversationId: json['conversation_id'] as int,
    isRead: json['is_read'] as bool,
    isReceived: json['is_received'] as bool,
    isSent: json['is_sent'] as bool,
    contentType: json['content_type'] as String,
    sender: json['sender'] == null
        ? null
        : Account.fromJson(json['sender'] as Map<String, dynamic>),
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'sender_id': instance.senderId,
      'conversation_id': instance.conversationId,
      'is_read': instance.isRead,
      'is_received': instance.isReceived,
      'is_sent': instance.isSent,
      'content_type': instance.contentType,
      'sender': instance.sender,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
    };
