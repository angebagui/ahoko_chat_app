// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Conversation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Conversation _$ConversationFromJson(Map<String, dynamic> json) {
  return Conversation(
    json['id'] as int,
    (json['speakers'] as List)?.map((e) => e as int)?.toList(),
    json['is_group'] as bool,
    json['group_name'] as String,
    (json['admins'] as List)?.map((e) => e as int)?.toList(),
    (json['speaker_list'] as List)?.map((e) => e as int)?.toList(),
    (json['users'] as List)
        ?.map((e) =>
            e == null ? null : Account.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['messages'] as List)
        ?.map((e) =>
            e == null ? null : Message.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$ConversationToJson(Conversation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'speakers': instance.speakers,
      'is_group': instance.isGroup,
      'group_name': instance.groupName,
      'admins': instance.admins,
      'speaker_list': instance.speakerList,
      'users': instance.users,
      'messages': instance.messages,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
    };
