import 'package:ahoko_chat_app/model/Account.dart';

import 'package:json_annotation/json_annotation.dart';
part 'Message.g.dart';

@JsonSerializable()
class Message{
  final int id;
  final String content;
  @JsonKey(name: 'sender_id')
  final int senderId;
  @JsonKey(name: 'conversation_id')
  final int conversationId;
  @JsonKey(name: 'is_read')
  final bool isRead;
  @JsonKey(name: 'is_received')
  final bool isReceived;
  @JsonKey(name: 'is_sent')
  final bool isSent;
  @JsonKey(name: 'content_type')
  final String contentType;
  final Account sender;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;


  const Message({
    this.id,
    this.content,
    this.senderId,
    this.conversationId,
    this.isRead,
    this.isReceived,
    this.isSent,
    this.contentType,
    this.sender,
    this.createdAt,
    this.updatedAt});

  factory Message.fromJson(Map<String, dynamic> json) => _$MessageFromJson(json);
  Map<String, dynamic> toJson() => _$MessageToJson(this);

  /*static Message fromJson(Map<String, dynamic> json) => Message(
    id: json['id'],
    content: json['content'],
    senderId: json['sender_id'],
    conversationId: json['conversation_id'],
    isRead: json['is_read'],
    isReceived: json['is_received'],
    isSent: json['is_sent'],
    contentType: json['content_type'],
    sender: json['sender'] == null? null: Account.fromJson(json['sender'] as Map),
    createdAt: json['created_at'] ==null? null: DateTime.parse(json['created_at']),
    updatedAt:  json['updated_at'] ==null? null:DateTime.parse(json['updated_at']),
  );*/

  @override
  String toString() {
    return 'Message{id: $id, content: $content, senderId: $senderId, conversationId: $conversationId, isRead: $isRead, isReceived: $isReceived, isSent: $isSent, contentType: $contentType, sender: $sender, createdAt: $createdAt, updatedAt: $updatedAt}';
  }
}