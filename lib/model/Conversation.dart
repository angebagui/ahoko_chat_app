import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Message.dart';
import 'package:intl/intl.dart';
import 'package:ahoko_chat_app/util/Constants.dart';
import 'package:flutter/material.dart';

import 'package:json_annotation/json_annotation.dart';
part 'Conversation.g.dart';

@JsonSerializable()
class Conversation{

  final int id;
  final List<int> speakers;
  @JsonKey(name: 'is_group')
  final bool isGroup;
  @JsonKey(name: 'group_name')
  final String groupName;
  final List<int> admins;
  @JsonKey(name: 'speaker_list')
  final List<int> speakerList;
  final List<Account> users;
  final List<Message> messages;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;

  const Conversation(this.id, this.speakers, this.isGroup, this.groupName,
    this.admins, this.speakerList, this.users, this.messages,   this.createdAt,
    this.updatedAt);

  factory Conversation.fromJson(Map<String, dynamic> json) => _$ConversationFromJson(json);
  Map<String, dynamic> toJson() => _$ConversationToJson(this);

/*  static Conversation fromJson(Map<String, dynamic> json) => Conversation(
    id: json['id'],
    speakers: json['speakers'] == null? null: (json['speakers'] as List).map<int>((e) => e as int).toList(),
    isGroup: json['is_group'],
    groupName:  json['group_name'],
    admins: json['admins'] == null? null: (json['admins'] as List).map<int>((e) => e as int).toList(),
    speakerList: json['speaker_list'] == null? null: (json['speaker_list'] as List).map<int>((e) => e as int).toList(),
    users: json['users'] == null? null: (json['users'] as List).map<Account>((json) => Account.fromJson(json as Map)).toList(),
    messages: json['messages'] == null? null: (json['messages'] as List).map<Message>((json) => Message.fromJson(json as Map)).toList(),
    createdAt: json['created_at'] ==null? null: DateTime.parse(json['created_at']),
    updatedAt:  json['updated_at'] ==null? null:DateTime.parse(json['updated_at']),
   );*/

  @override
  String toString() {
    return 'Conversation{id: $id, speakers: $speakers, isGroup: $isGroup, groupName: $groupName, admins: $admins, speakerList: $speakerList, users: $users, messages: $messages, createdAt: $createdAt, updatedAt: $updatedAt}';
  }

  Account getReceiver(Account me){

    if(users == null || users.isEmpty) return null;

    List<Account> items = users.where((element) => element.id != me.id).toList();

    if(items.isEmpty) return null;

    return items.first;

  }

  String title(Account me){
    if(isGroup){
      return groupName == null? 'Group inconnu':groupName;
    }else{
      final receiver = getReceiver(me);
      return receiver == null? 'Inconnu': "${receiver.firstName} ${receiver.lastName}";
    }
  }

  String avatar(Account me){
    if(isGroup){
      return "https://cdn4.iconfinder.com/data/icons/social-media-3/512/User_Group-512.png";
    }else{
      final receiver = getReceiver(me);
      return receiver == null || receiver.photo == null || receiver.photo.isEmpty? defaultAvatar:
      receiver.photo;
    }
  }

  String lastMessageContent() {

    if(messages == null || messages.isEmpty) return '';

    final message = messages.first;

    if(message.contentType == 'text')
      return message.content;
    else
      return 'Media envoyé';

  }

  String lastMessageTime(Locale locale) {

    if(messages == null || messages.isEmpty) return '';

    final message = messages.first;

    return DateFormat.Hm(locale.languageCode).format(message.createdAt);

  }

  int unReadMessagesCount() {

    if(messages == null || messages.isEmpty) return 0;

    final unReadMessages = messages.where((element) => element.isRead == false || element.isRead == null).toList();

    return unReadMessages.length;

  }

}