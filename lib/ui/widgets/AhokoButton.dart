import 'package:flutter/material.dart';

class AhokoButton extends StatelessWidget {
  final String title;
  final void Function() onTap;
  final Color color;
  const AhokoButton({@required this.title,@required this.onTap, this.color}) ;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      child: ElevatedButton(
        onPressed: this.onTap,
        child: Text(this.title),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(this.color == null? Theme.of(context).primaryColor : this.color  ),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)))
        ),
      ),
    );
  }
}
