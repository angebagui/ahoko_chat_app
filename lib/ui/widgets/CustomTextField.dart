import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final TextInputType keyboardType;
  final bool obscureText;
  final TextEditingController controller;
  const CustomTextField({
    this.hint = '',
    @required this.keyboardType,
    this.obscureText = false,
    @required this.controller
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: this.controller,
      keyboardType: this.keyboardType,
      obscureText: this.obscureText,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        hintText: this.hint,
      ),
    );
  }
}
