import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/util/AhokoColors.dart';
import 'package:flutter/material.dart';

class ConversationWidget extends StatelessWidget {
  final Conversation conversation;
  final Account me;

  const ConversationWidget({
    @required this.conversation,
    @required this.me
  });

  @override
  Widget build(BuildContext context) {

    final Locale locale = Localizations.localeOf(context);

    return  ListTile(
      leading: Container(
        width: 60,
        height: 60,
        child: Stack(
          children: [
            Container(
              height: 60.0 ,
              width: 60.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(conversation.avatar(me))
                  ),
                  shape: BoxShape.circle
              ),
            ),

            Align(
              alignment: Alignment.bottomLeft,
              child:  Container(
                height: 15.0 ,
                width: 15.0,
                decoration: BoxDecoration(
                    color: AhokoColors.onlineUserColor,
                    shape: BoxShape.circle,
                    border: Border.all(
                        color: Colors.white,
                        width: 4.0
                    )
                ),
              ),
            )


          ]
          ,),),
      title: Text(conversation.title(me), style: Theme.of(context).textTheme.headline6.copyWith(
          fontSize: 16

      ),),
      subtitle: Text(conversation.lastMessageContent(), style: Theme.of(context).textTheme.subtitle1,),
      trailing:   Container(
        width: 60,
        height: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(conversation.lastMessageTime(locale), style: TextStyle(color: AhokoColors.conversationTimeColor, fontSize: 14),),
            SizedBox(
              height: 8.0,
            ),
            Offstage(
              offstage: conversation.unReadMessagesCount() == 0,
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(10.0)
                ),
                padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0, bottom: 2.0),
                child: Text("${conversation.unReadMessagesCount()}", style: TextStyle(color: Colors.white, fontSize: 14),),
              ),
            )
          ],
        ),
      ),
    );
  }
}

