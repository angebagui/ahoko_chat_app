import 'package:flutter/material.dart';

class AhokoCircularProgressWidget extends StatelessWidget {

  const AhokoCircularProgressWidget();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: CircularProgressIndicator(
          color: Theme.of(context).primaryColor,
        ),
      ),

    );
  }
}
