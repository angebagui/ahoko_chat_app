import 'dart:io';
import 'package:ahoko_chat_app/CompositionRoot.dart';
import 'package:ahoko_chat_app/rest/exception/AuthException.dart';
import 'package:ahoko_chat_app/state_management/login/LoginBloc.dart';
import 'package:ahoko_chat_app/state_management/login/LoginEvent.dart';
import 'package:ahoko_chat_app/state_management/login/LoginState.dart';
import 'package:ahoko_chat_app/ui/auth/register/RegisterScreen.dart';
import 'package:ahoko_chat_app/ui/home/HomeScreen.dart';
import 'package:ahoko_chat_app/ui/widgets/AhokoButton.dart';
import 'package:ahoko_chat_app/ui/widgets/AhokoCircularProgressWidget.dart';
import 'package:ahoko_chat_app/ui/widgets/CustomTextField.dart';
import 'package:ahoko_chat_app/util/AhokoIcons.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {

  const LoginScreen();

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    context.read<LoginBloc>().stream.listen((LoginState state) {

      if(state is LoginProgress){
        _showProgress();
      }

      if(state is LoginSucceeded){
        _hideProgress();

        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> CompositionRoot.homeScreen(
          state.account
        )));

      }

      if(state is LoginFailed){
        _hideProgress();
        if(state.error is SocketException){
          //Display internet error
          print("Error =>>> Please verify internet network");
          Toast.show('Please verify Internet network !', context,duration: Toast.LENGTH_LONG);
        } else if(state.error is AuthException){
          //Display authentication error
          print("Error =>>> ${state.error.message}");
          Toast.show('${state.error.message}', context,duration: Toast.LENGTH_LONG);
        }else{
          //Display internal error
          print("Error =>>> Internal error found ${state.error}");
          Toast.show('Internal eror found', context,duration: Toast.LENGTH_LONG);
        }

      }

    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [

                  SizedBox(height: 50.0,),
                  Center(
                    child: AhokoIcons.logo,
                  ),
                  SizedBox(height: 50.0,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: CustomTextField(
                      hint: 'Email',
                      keyboardType: TextInputType.emailAddress,
                      controller: _emailController,
                    ) ,
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: CustomTextField(
                      hint: 'Password',
                      keyboardType: TextInputType.emailAddress,
                      controller: _passwordController,
                      obscureText: true,
                    ) ,
                  ),
                  SizedBox(height: 20.0,),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: AhokoButton(
                      title: 'Login',
                      onTap: _login,
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 16.0),

                    child: AhokoButton(
                      title: 'Register',
                      color: Theme.of(context).accentColor,
                      onTap: _register,
                    ),
                  )

                ],
              ),
            ),
          ),
          Offstage(
            offstage: !_isLoading,
            child: AhokoCircularProgressWidget(),
          )
        ],
      ),
    );
  }

  void _showProgress(){
    if(mounted){
      setState(() {
        _isLoading = true;
      });
    }
  }

  void _hideProgress(){
    if(mounted){
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _login(){
    final String email = this._emailController.text;
    final String password = this._passwordController.text;

    if(email.isEmpty){
      // Display an error for email required
      Toast.show('Email is required', context,duration: Toast.LENGTH_LONG);
      return;
    }

    if(password.isEmpty){
      // Display an error for password required
      Toast.show('Password is required', context,duration: Toast.LENGTH_LONG);
      return;
    }

    context.read<LoginBloc>().add(LoginSubmission(email,password));


  }

  void _register() {

      final String email = this._emailController.text;
      final String password = this._passwordController.text;

      Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context)=> CompositionRoot.registerScreen(
              email:email,
              password:password ))
      );

  }
}
