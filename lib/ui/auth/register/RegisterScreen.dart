import 'dart:io';

import 'package:ahoko_chat_app/CompositionRoot.dart';
import 'package:ahoko_chat_app/rest/exception/AuthException.dart';
import 'package:ahoko_chat_app/state_management/register/RegisterBloc.dart';
import 'package:ahoko_chat_app/state_management/register/RegisterEvent.dart';
import 'package:ahoko_chat_app/state_management/register/RegisterState.dart';
import 'package:ahoko_chat_app/ui/auth/login/LoginScreen.dart';
import 'package:ahoko_chat_app/ui/home/HomeScreen.dart';
import 'package:ahoko_chat_app/ui/widgets/AhokoButton.dart';
import 'package:ahoko_chat_app/ui/widgets/AhokoCircularProgressWidget.dart';
import 'package:ahoko_chat_app/ui/widgets/CustomTextField.dart';
import 'package:ahoko_chat_app/util/AhokoIcons.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

class RegisterScreen extends StatefulWidget {
  final String email;
  final String password;
  const RegisterScreen({
    this.email,
    this.password
  });

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  bool _isLoading = false;

  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();

    print("Email: ${widget.email}");
    print("Password: ${widget.password}");

    if(widget.email != null && widget.email.isNotEmpty){
      _emailController.text = widget.email;
    }

    if(widget.password != null && widget.password.isNotEmpty){
      _passwordController.text = widget.password;
    }


    context.read<RegisterBloc>().stream.listen((RegisterState state) {

      if(state is RegisterProgress){
        _showProgress();
      }

      if(state is RegisterSucceeded){
        _hideProgress();

        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> CompositionRoot.homeScreen(
            state.account
        )));

      }

      if(state is RegisterFailed){
        _hideProgress();
        if(state.error is SocketException){
          //Display internet error
          print("Error =>>> Please verify internet network");
          Toast.show('Please verify Internet network !', context,duration: Toast.LENGTH_LONG);
        } else if(state.error is AuthException){
          //Display authentication error
          print("Error =>>> ${state.error.message}");
          Toast.show('${state.error.message}', context,duration: Toast.LENGTH_LONG);
        }else{
          //Display internal error
          print("Error =>>> Internal error found ${state.error}");
          Toast.show('Internal eror found', context,duration: Toast.LENGTH_LONG);
        }

      }

    });



  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [

                  SizedBox(height: 50.0,),
                  Center(
                    child: AhokoIcons.logo,
                  ),
                  SizedBox(height: 50.0,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: CustomTextField(
                      hint: 'Firstname',
                      keyboardType: TextInputType.name,
                      controller: _firstNameController,
                    ) ,
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: CustomTextField(
                      hint: 'Lastname',
                      keyboardType: TextInputType.name,
                      controller: _lastNameController,
                    ) ,
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: CustomTextField(
                      hint: 'Email',
                      keyboardType: TextInputType.emailAddress,
                      controller: _emailController,
                    ) ,
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: CustomTextField(
                      hint: 'Password',
                      keyboardType: TextInputType.text,
                      controller: _passwordController,
                      obscureText: true,
                    ) ,
                  ),
                  SizedBox(height: 20.0,),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: AhokoButton(
                      title: 'Register',
                      onTap: _register,
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 16.0),

                    child: AhokoButton(
                      title: 'Login',
                      color: Theme.of(context).accentColor,
                      onTap: (){

                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context)=>CompositionRoot.loginScreen())
                        );

                      },
                    ),
                  )

                ],
              ),
            ),
          ),
          Offstage(
            offstage: !_isLoading,
            child: AhokoCircularProgressWidget(),
          )
        ],
      ),
    );
  }


  void _showProgress(){
    if(mounted){
      setState(() {
        _isLoading = true;
      });
    }
  }

  void _hideProgress(){
    if(mounted){
      setState(() {
        _isLoading = false;
      });
    }
  }

  _register ()async{
    final String email = this._emailController.text;
    final String password = this._passwordController.text;
    final String firstName = this._firstNameController.text;
    final String lastName = this._lastNameController.text;

    if(email.isEmpty){
      // Display an error for email required
      Toast.show('Email is required', context,duration: Toast.LENGTH_LONG);
      return;
    }

    if(password.isEmpty){
      // Display an error for password required
      Toast.show('Password is required', context,duration: Toast.LENGTH_LONG);
      return;
    }

    if(firstName.isEmpty){
      // Display an error for password required
      Toast.show('Firstname is required', context,duration: Toast.LENGTH_LONG);
      return;
    }

    if(lastName.isEmpty){
      // Display an error for password required
      Toast.show('Lastname is required', context,duration: Toast.LENGTH_LONG);
      return;
    }

    context.read<RegisterBloc>().add(RegisterSubmission(email, password, firstName, lastName));


  }
}
