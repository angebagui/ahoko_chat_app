import 'dart:io';

import 'package:ahoko_chat_app/CompositionRoot.dart';
import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/state_management/conversation/conversation_bloc.dart';
import 'package:ahoko_chat_app/state_management/conversation/conversation_event.dart';
import 'package:ahoko_chat_app/state_management/conversation/conversation_state.dart';
import 'package:ahoko_chat_app/ui/home/tabs/GroupScreen.dart';
import 'package:ahoko_chat_app/util/AhokoIcons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

class HomeScreen extends StatefulWidget {
  final Account me;
  HomeScreen(this.me);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentPosition = 0;

  @override
  void initState() {
    super.initState();

    context.read<ConversationBloc>().stream.listen((state) {

      if(state is ConversationFound){

        final route = MaterialPageRoute(builder: (context)=> CompositionRoot.conversationScreen(state.conversation, widget.me));

        Navigator.push(context, route);
      }

      if(state is Failure){
        var  message = '';
        final error = state.error;

        if(error is Response){
          if(error.statusCode >= 500) {
            message = "Internal Server Error";
          }else{
            message = "Bad Response";
          }

        }

        if(error is SocketException){
          message = "Vérifiez votre connection internet !";
        }

        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }


    });

  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Search'),
            bottom: TabBar(
              tabs: [

                Tab(
                  text: "Message",
                ),
                Tab(
                  text: 'Group',
                ),

              ],
            ),

          ),
          body: TabBarView(
            children: [

               CompositionRoot.messageScreen(widget.me),
               GroupScreen()

            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: ()async{

              final Account contact = await Navigator.push(context, MaterialPageRoute(builder: (_)=> CompositionRoot.listContactScreen(widget.me)));
              print("Selected user: $contact");
              if(contact != null){
                _openConversation(contact);
              }


            },
            child: Icon(Icons.add),
          ),
          bottomNavigationBar: BottomNavigationBar(
            //type: BottomNavigationBarType.fixed,
            currentIndex: _currentPosition,
            onTap: (int index){

              if(mounted){
                setState(() {
                  _currentPosition = index;

                  print("Position $index");
                  print("CurrentPosition $_currentPosition");
                });
              }

            },
            items: [
              BottomNavigationBarItem(
                  icon: AhokoIcons.message,
                  activeIcon: AhokoIcons.message_active,
                  label: ''),
              BottomNavigationBarItem(
                  icon: AhokoIcons.friends,
                  activeIcon: AhokoIcons.friends_active,
                  label: ''),
              BottomNavigationBarItem(
                  icon: AhokoIcons.add_box,
                  activeIcon: AhokoIcons.add_box_active,
                  label: ''),
              BottomNavigationBarItem(
                  icon:  Stack(
                    alignment: Alignment.topRight,
                    children: [
                      AhokoIcons.notifications,
                      Container(
                        width: 22,
                        height: 22,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.black,
                        ),
                        child: Text("20", style: TextStyle(
                          color: Colors.white,
                          fontSize: 12
                        ),),
                      )
                    ],
                  ),
                  activeIcon: Stack(
                    alignment: Alignment.topRight,
                    children: [
                      AhokoIcons.notifications_active,
                      Container(
                        width: 22,
                        height: 22,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.black,
                        ),
                        child: Text("20", style: TextStyle(
                            color: Colors.white,
                            fontSize: 12
                        ),),
                      )
                    ],
                  ),
                  label: ''),
              BottomNavigationBarItem(icon: AhokoIcons.profile,
                  activeIcon: AhokoIcons.profile_active,
                  label: ''),
            ],
          ),
        )
    );
  }

  void _openConversation(Account contact) {

    final speakers = [widget.me.id, contact.id];
    context.read<ConversationBloc>().add(FindConversation(speakers));

  }

}

