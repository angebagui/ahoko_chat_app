import 'package:ahoko_chat_app/CompositionRoot.dart';
import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/ui/widgets/ConversationWidget.dart';
import 'package:flutter/material.dart';
import 'package:ahoko_chat_app/state_management/messages/message_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MessageScreen extends StatefulWidget {
  final Account me;
  const MessageScreen(this.me);

  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {

  @override
  void initState() {
    super.initState();

    if(widget.me != null){
      context.read<MessageBloc>().loadConversations(widget.me.id);
    }

  }

  @override
  Widget build(BuildContext context) {
    return  BlocBuilder<MessageBloc, List<Conversation>>(
      builder: (_, state){
        return state == null? Container():SafeArea(
            child: ListView(
              children: state.map<Widget>((conversation) =>  InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> CompositionRoot.conversationScreen(conversation, widget.me)));
                },
                child: ConversationWidget(
                  conversation: conversation,
                  me: widget.me,
                ),
              )).toList(),
            )
        );
      },
    );
  }
}
