import 'dart:io';

import 'package:ahoko_chat_app/model/Account.dart';
import 'package:ahoko_chat_app/model/Conversation.dart';
import 'package:ahoko_chat_app/model/Message.dart';
import 'package:ahoko_chat_app/state_management/conversation/conversation_bloc.dart';
import 'package:ahoko_chat_app/state_management/conversation/conversation_event.dart';
import 'package:ahoko_chat_app/state_management/conversation/conversation_state.dart';
import 'package:ahoko_chat_app/ui/widgets/CustomTextField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class ConversationScreen extends StatefulWidget {

  final Conversation conversation;
  final Account me;
  const ConversationScreen(this.conversation, this.me);

  @override
  _ConversationScreenState createState() => _ConversationScreenState();
}

class _ConversationScreenState extends State<ConversationScreen> {

  TextEditingController _inputController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    context.read<ConversationBloc>().stream.listen((state) {

      if(state is MessageSent){
        _loadMessages();
      }
      if(state is Failure){
        var  message = '';
        final error = state.error;

        if(error is Response){
          if(error.statusCode >= 500) {
            message = "Internal Server Error";
          }else{
            message = "Bad Response";
          }

        }

        if(error is SocketException){
          message = "Vérifiez votre connection internet !";
        }

        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }
    });

    _loadMessages();


  }


  _loadMessages(){
    context.read<ConversationBloc>().add(FindMessagesByConversation(widget.conversation.id));
  }

  _sendMessageText(String text){
    final Message message = Message(
        content: text,
        contentType: 'text',
        senderId: widget.me.id,
        conversationId: widget.conversation.id
    );
    context.read<ConversationBloc>().add(SendMessage(message));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.conversation.title(widget.me)}"),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody(){

    return Column(
      children: [

        _buildMessageList(),
        _buildInputText()
      ],
    );
  }


  Widget _buildInMessageBox(Message message) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 10.0, top: 10.0),
          width: 50.0,
          height: 50.0,
          decoration: BoxDecoration(
            color: Colors.black,
            shape: BoxShape.circle,
            /*image: DecorationImage(
              image: NetworkImage()
            )*/
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width - 150,
          padding: EdgeInsets.all(16.0),
          margin: EdgeInsets.only( top: 10.0, right: 80.0, left: 10.0),
          decoration: BoxDecoration(
              color: Color(0xFFE2E2E4),
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
              )
          ),
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [

              message.contentType == 'text'?Container(
                padding:EdgeInsets.only(bottom: 20.0),
                child: Text(message.content == null? '':message.content,
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                      color: Colors.white
                  ),),
              ):Container(
                padding:EdgeInsets.only(bottom: 20.0),
                child: Text("Media content",style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Colors.white
                )),
              ),

              Align(
                alignment: Alignment.bottomRight,
                child: Text("${DateFormat.Hm().format(message.createdAt)}", style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Colors.black
                )),
              )
            ],
          ),
        )
      ],
    );
  }
  Widget _buildOutMessageBox(Message message) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          padding: EdgeInsets.all(16.0),
          margin: EdgeInsets.only(right: 10.0, top: 10.0, left: 80.0),
          decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
              )
          ),
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [

              message.contentType == 'text'?Container(
                padding:EdgeInsets.only(bottom: 20.0),
                child: Text(message.content == null? '':message.content,
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                      color: Colors.white
                  ),),
              ):Container(
                padding:EdgeInsets.only(bottom: 20.0),
                child: Text("Media content",style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Colors.white
                )),
              ),

              Align(
                alignment: Alignment.bottomRight,
                child: Text("${DateFormat.Hm().format(message.createdAt)}", style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Colors.white
                )),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _buildInputText() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: 20.0, top: 10.0, right: 20.0, bottom: 20.0),
      child: Row(
        children: [

          Expanded(
            child: CustomTextField(
              hint: 'Exprimez vous',
              controller: _inputController,
              keyboardType: TextInputType.multiline,
            ),
          ),
          GestureDetector(
            onTap: (){
              final messageText = _inputController.text;
              _inputController.clear();
              _sendMessageText(messageText);
            },
            child: Container(
              width: 60.0,
              height: 60.0,
              margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
                /*image: DecorationImage(
              image: NetworkImage()
            )*/
              ),
              child: Icon(Icons.send, color: Colors.white),
            ),
          ),

        ],
      ),
    ) ;
  }

  _buildMessageList() {
    return Expanded(
      child: BlocBuilder<ConversationBloc, ConversationState>(
        builder: (_, state){
          if(state is MessagesFound){
            return ListView(
                children: state.messages.map<Widget>((message) =>
                message.senderId == widget.me.id?
                _buildOutMessageBox(message)
                    : _buildInMessageBox(message)).toList()
            );
          }else{
            return Container();
          }

        },
      ),
    );
  }
}
